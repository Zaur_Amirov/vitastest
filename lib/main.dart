import 'package:barcode_scan/platform_wrapper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttervitastest/ticket.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(userName: 'Голобородько Данило'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key, this.userName}) : super(key: key);

  final String userName;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF1B2742),
        body: new Column(children: [
          new SizedBox(
            height: 30.0,
          ),
          Container(
              padding: const EdgeInsets.all(10.0),
              child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    new Text(userName,
                        style: TextStyle(
                            fontSize: 16.0,
                            color: new Color(0xFFA4ADC2),
                            fontFamily: 'GothamPro')),
                    new Text("Вийти",
                        style: TextStyle(
                            fontSize: 16.0, color: new Color(0xFF0775EC)))
                  ])),
          Container(
              padding: const EdgeInsets.all(16.0),
              child: Center(
                  child: new Text("Оберіть режим роботи",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: new Color(0xFFFFFFFF),
                          fontSize: 38.0,
                          fontFamily: 'GothamPro')))),
          new GestureDetector(
              onTap: () {
                print("Container 1 clicked");
                scan();
              },
              child: new Container(
                  child: Container(
                child: new Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Container(
                        padding: const EdgeInsets.all(16.0),
                        child: new SvgPicture.asset(
                          'assets/images/ic_qrscunning.svg',
                          width: 48.0,
                          height: 48.0,
                        )),
                    new Text("Сканування квитків", style: new TextStyle(fontSize: 22.0),),
                  ],
                ),
                margin: const EdgeInsets.fromLTRB(32.0, 16.0, 32.0, 16.0),
                color: Colors.white,
                height: 120,
                width: double.infinity,
              ))),
          new GestureDetector(
              onTap: () {
                print("Container 2 clicked");
                Navigator.push(context, MaterialPageRoute(builder: (context) => TicketScreen(userName: 'Голобородько Данило')));
              },
              child: new Container(
                  child: Container(
                margin: const EdgeInsets.fromLTRB(32.0, 16.0, 32.0, 16.0),
                color: Colors.white,
                height: 120,
                width: double.infinity,
                child: new Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Container(
                        padding: const EdgeInsets.all(16.0),
                        child: new SvgPicture.asset(
                          'assets/images/master-card.svg',
                          width: 65.0,
                          height: 48.0,
                        )),
                    new Text("Продаж квитків", style: new TextStyle(fontSize: 22.0),),
                  ],
                ),
              ))),
        ]));
  }

  void scan() async {
    var result = await BarcodeScanner.scan();

    print(result.type); // The result type (barcode, cancelled, failed)
    print(result.rawContent); // The barcode content
    print(result.format); // The barcode format (as enum)
    print(result.formatNote); // If a unknown format was scanned this field contains a note
  }
}
