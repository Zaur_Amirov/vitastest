import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttervitastest/fare_payment.dart';

class TicketScreen extends StatefulWidget {
  TicketScreen({Key key, this.userName}) : super(key: key);

  final String userName;

  @override
  createState() => _TicketScreenState(userName);
}

class _TicketScreenState extends State<TicketScreen> {
  _TicketScreenState(this.userName);

  String dropdownValue = 'Студентський';
  String dropdownValueDuration = '6 мiсяцiв';
  final String userName;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF1B2742),
        body: new Column(children: [
          new SizedBox(
            height: 30.0,
          ),
          Container(
              padding: const EdgeInsets.all(10.0),
              child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    new Text(userName,
                        style: TextStyle(
                            fontSize: 16.0,
                            color: new Color(0xFFA4ADC2),
                            fontFamily: 'GothamPro')),
                    new Text("Вийти",
                        style: TextStyle(
                            fontSize: 16.0, color: new Color(0xFF0775EC)))
                  ])),
          Align(
              alignment: Alignment.centerLeft,
              child: new Container(
                  padding: const EdgeInsets.fromLTRB(16.0, 16.0, 0, 0),
                  child: new Text("Абонемент",
                      style: TextStyle(
                          color: new Color(0xFFFFFFFF),
                          fontSize: 38.0,
                          fontFamily: 'GothamPro')))),
          Align(
              alignment: Alignment.centerLeft,
              child: new Container(
                  padding: const EdgeInsets.fromLTRB(16.0, 16.0, 0, 0),
                  child: new Text("Тип",
                      style: TextStyle(
                          color: new Color(0xFFA4ADC2),
                          fontSize: 20.0,
                          fontFamily: 'GothamPro')))),
          Align(
              alignment: Alignment.centerLeft,
              child: new Container(
                  padding: const EdgeInsets.fromLTRB(16.0, 0, 0, 0),
                  child: DropdownButton<String>(
                    value: dropdownValue,
                    icon: Icon(Icons.arrow_downward),
                    iconSize: 24,
                    elevation: 16,
                    style: TextStyle(color: Colors.black),
                    underline: Container(
                      height: 2,
                      color: Colors.white,
                    ),
                    onChanged: (String newValue) {
                      setState(() {
                        dropdownValue = newValue;
                      });
                    },
                    items: <String>[
                      'Дитячий',
                      'Студентський',
                      'Дорослий',
                      'Пенсiйний'
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ))),
          Align(
              alignment: Alignment.centerLeft,
              child: new GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                FarePaymentWidget(userName: userName)));
                  },
                  child: new Container(
                      padding: const EdgeInsets.fromLTRB(16.0, 16.0, 0, 0),
                      child: new Text("Тривалість",
                          style: TextStyle(
                              color: new Color(0xFFA4ADC2),
                              fontSize: 20.0,
                              fontFamily: 'GothamPro'))))),
          Align(
              alignment: Alignment.centerLeft,
              child: new Container(
                  padding: const EdgeInsets.fromLTRB(16.0, 0, 0, 0),
                  child: DropdownButton<String>(
                    value: dropdownValueDuration,
                    icon: Icon(Icons.arrow_downward),
                    iconSize: 24,
                    elevation: 16,
                    style: TextStyle(color: Colors.black),
                    underline: Container(
                      height: 2,
                      color: Colors.white,
                    ),
                    onChanged: (String newValue) {
                      setState(() {
                        dropdownValueDuration = newValue;
                      });
                    },
                    items: <String>[
                      '1 поездка',
                      '1 мiсяць',
                      '3 мiсяца',
                      '6 мiсяцiв'
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ))),
          Align(
              alignment: Alignment.centerLeft,
              child: new Container(
                  padding: const EdgeInsets.fromLTRB(16.0, 16.0, 0, 16),
                  child: new Row(
                    children: <Widget>[
                      new GestureDetector(
                          onTap: () {
                            print("Container 3 clicked");
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        FarePaymentWidget(userName: userName)));
                          },
                          child: new Container(
                            color: new Color(0xFFE2EBFD),
                            padding:
                                const EdgeInsets.fromLTRB(16.0, 16.0, 16, 16),
                            child: new Row(children: <Widget>[
                              new SvgPicture.asset(
                                'assets/images/hryvnya.svg',
                                width: 24.0,
                                height: 24.0,
                              ),
                              new Container(
                                  padding:
                                      const EdgeInsets.fromLTRB(8.0, 0.0, 0, 0),
                                  child: new Text("Готiвка",
                                      style: TextStyle(
                                          color: new Color(0xFF000000),
                                          fontSize: 20.0,
                                          fontFamily: 'GothamPro')))
                            ]),
                          ))
                    ],
                  ))),
          new Expanded(
              child: Container(
                  padding: const EdgeInsets.fromLTRB(16.0, 0, 12, 16),
                  child: new Align(
                      alignment: Alignment.bottomCenter,
                      child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RaisedButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              color: new Color(0xFF49587C),
                              child: const Text('Назад',
                                  style: TextStyle(
                                      fontSize: 16.0, color: Colors.white)),
                            ),
                            RaisedButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              color: new Color(0xFF49587C),
                              child: new SvgPicture.asset(
                                'assets/images/home.svg',
                                width: 24.0,
                                height: 24.0,
                              ),
                            ),
                          ]))))
        ]));
  }
}
