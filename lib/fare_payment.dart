import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FarePaymentWidget extends StatelessWidget {

  FarePaymentWidget({Key key, this.userName}) : super(key: key);

  final String userName;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF1B2742),
        body: new Column(children: [
          new SizedBox(
            height: 30.0,
          ),

          Container(
              padding: const EdgeInsets.all(10.0),
              child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    new Text(userName,
                        style: TextStyle(
                            fontSize: 16.0,
                            color: new Color(0xFFA4ADC2),
                            fontFamily: 'GothamPro')),
                    new Text("Вийти",
                        style: TextStyle(
                            fontSize: 16.0, color: new Color(0xFF0775EC)))
                  ])),
          Align(
              alignment: Alignment.centerLeft,
              child: new Container(
                  padding: const EdgeInsets.fromLTRB(16.0, 16.0, 0, 0),
                  child: new Text("Оплата готівкою",
                      style: TextStyle(
                          color: new Color(0xFFFFFFFF),
                          fontSize: 38.0,
                          fontFamily: 'GothamPro')))),
          new Expanded(
              child: Container(
                  padding: const EdgeInsets.fromLTRB(16.0, 0, 12, 16),
                  child: new Align(
                      alignment: Alignment.bottomCenter,
                      child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RaisedButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              color: new Color(0xFF49587C),
                              child: const Text('Назад',
                                  style: TextStyle(
                                      fontSize: 16.0, color: Colors.white)),
                            ),
                            RaisedButton(
                              onPressed: () {
                                Navigator.popUntil(context, ModalRoute.withName(Navigator.defaultRouteName));
                              },
                              color: new Color(0xFF49587C),
                              child: new SvgPicture.asset(
                                'assets/images/home.svg',
                                width: 24.0,
                                height: 24.0,
                              ),
                            ),
                          ]))))
        ]));
  }
}
